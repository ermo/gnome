# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2012 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require gtk-vnc
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SLOT="2.0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    gobject-introspection
    pulseaudio [[ description = [ VNC audio tunel support ] ]]
    sasl
    vapi [[ description = [ Install vala bindings ] requires = [ gobject-introspection ] ]]
    ( linguas: ca ca@valencia cs da de el en_GB eo es eu fr gl he hu id it ja ko lt lv nb nds pa pl
               pt pt_BR ro ru sk sl sr sr@latin sv tg th tr uk zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-perl/Text-CSV
        dev-util/intltool[>=0.40.0]
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=0.9.4] )
        vapi? ( dev-lang/vala:*[>=0.14.1] ) [[ note = [ tested with 0.14 and up ] ]]
    build+run:
        dev-libs/glib:2[>=2.10.0]
        dev-libs/gnutls[>=3.0]
        dev-libs/libgcrypt[>=1.4.0]
        x11-libs/cairo[>=1.2.0]
        x11-libs/gdk-pixbuf:2.0[>=2.10.0]
        x11-libs/libX11
        x11-libs/gtk+:3[>=3.0.0]
        pulseaudio? ( media-sound/pulseaudio )
        sasl? ( net-libs/cyrus-sasl )
        !dev-libs/gtk-vnc:1 [[
            description = [ Libraries and binaries collide with these slots ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=( '--with-coroutine=gthread' '--with-gtk=3.0' '--without-libview' )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( pulseaudio sasl )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' 'vapi vala' )

src_prepare() {
    edo sed -i configure.ac -e "s/ld --help/${LD} --help/"
    autotools_src_prepare
    edo intltoolize --force --automake
}

