# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache freedesktop-desktop meson

SUMMARY="Color profile manager for the GNOME desktop"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    exiv [[ description = [ Enable EXIV support for RAW support ] ]]
"

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/glib:2[>=2.31.10]
        dev-libs/vte:2.91[>=0.25.1] [[ note = [ automagic dependency ] ]]
        gnome-desktop/colord-gtk[>=0.1.22]
        media-libs/lcms2[>=2.2]
        media-libs/libcanberra[>=0.10][providers:gtk3]
        media-libs/libexif
        media-libs/tiff
        sys-apps/colord[>=1.3.1]
        x11-libs/gtk+:3[>=2.91]
        exiv? ( graphics/exiv2 )
"

# Need access to X/Wayland
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dpackagekit=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    exiv
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_install() {
    meson_src_install

    # GCM_SYSTEM_PROFILES_DIR
    keepdir /var/lib/color
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

